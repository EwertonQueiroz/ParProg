use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io;

pub fn ler_arquivo(path: &str) -> Result<(), Box<Error>> {
	let mut file = File::open(path)?;

	let mut conteudo = String::new();
	
	file.read_to_string(&mut conteudo)?;

	Ok(())
}


pub fn ler (path: &str) -> io::Result<()> {
	let file = File::open(path)?;
	
	let reader = io::BufReader::new(file);
	
	for line in reader.lines() {
		let line = line?;
		println!("{}", line);
	}
	
	Ok(())
}


pub fn parse_path(argv: &[String]) -> Result<&str, &'static str> {
	// Tem que receber os 4 arquivos de uma vez
	// if argv.len() < 4 {
	if argv.len() < 2 {
		// Tratamento de erro
		return Err("Faltam arquivos de entrada");
	}

	let path = &argv[1];

	Ok(path)
}


/*
pub struct Mao {
	pub carta1: String,
	pub carta2: String,
	pub carta3: String,
	pub carta4: String,
	pub carta5: String,
}


pub impl Mao {
	pub fn new(line: &[String]) -> Mao {
		let cartas[5];

		cartas[0] = line.split(' ').get(0).clone();
	
		Mao { cartas[0], ..., cartas[4] }
	}
}
*/

#[derive(Copy, Clone)]
enum Carta {
	T = 10,
	J,
	Q,
	K,
	A,
}


pub fn identificarPadrao (linha: &mut [String]) {
	let mut quatroIguais: u32;
	let mut todosDiferentes: u32;
	let mut sequencias: u32;
	let mut count: u8;

	linha.sort();

	for x in linha {
		//for j in x {
			println!("{}", x);
		//}
	}


pub fn tipo<T>(_: &T) -> () {
	let nome = unsafe { (*std::intrinsics::get_tydesc::<T>()).name };
	println!("{}", nome);
}


/*
 = vec![Carta::K as u8, 9, 8, Carta::T as u8, 7, 2, 3, Carta::A as u8, 0, Carta::Q as u8, 1, 4, 6, 5, Carta::J as u8];

let mut l2 = linha.clone();
	l2.sort();

	for x in linha {
		print!("{} ", x);
	}

	println!("");

	for x in l2 {
		print!("{} ", x);
	}

	println!("");
*/
}

