extern crate Projeto_Paradigmas;

use std::env;
use std::process;

//use poker::Mao;

fn main() {
	// Generics no Vetor Vec<?>
	let argv: Vec<String> = env::args().collect();

	// Modularização e tratamento de erros
	let path = Projeto_Paradigmas::parse_path(&argv).unwrap_or_else(|err| {
		println!("Faltam argumentos: {}", err);
		process::exit(1);
	});
/*
	if let Err(e) = Projeto_Paradigmas::ler_arquivo(path) {
		println!("Erro de execucao: {}", e);
		process::exit(1);
	}
*/

	let arquivo = Projeto_Paradigmas::ler(path);

	tipo(&arquivo);	

//	Projeto_Paradigmas::identificarPadrao();
}
